package com.example.aidavianisa.appberumon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Group extends AppCompatActivity implements View.OnClickListener{

    Button board;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        board = (Button) findViewById(R.id.board);
        board.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_home) {
            startActivity(new Intent(this, MainActivity.class));
            return true;
        }
        if (id == R.id.action_group) {
            startActivity(new Intent(this, Group.class));
            return true;
        }
        if (id == R.id.action_search) {
            startActivity(new Intent(this, Search.class));
            return true;
        }
        if (id == R.id.action_message) {
            startActivity(new Intent(this, MessageNotification.class));
            return true;
        }
        if (id == R.id.action_profile) {
            startActivity(new Intent(this, MainActivity.class));
            return true;
        }
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, MainActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.board:
                startActivity(new Intent(this, GroupUser.class));
                break;
        }
    }
}
