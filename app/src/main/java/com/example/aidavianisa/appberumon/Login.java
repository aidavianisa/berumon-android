package com.example.aidavianisa.appberumon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login extends AppCompatActivity implements View.OnClickListener{

    Button button_signin, button_goRegister;
    EditText input_email, input_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        input_email = (EditText) findViewById(R.id.input_email);
        input_password = (EditText) findViewById(R.id.input_password);
        button_signin = (Button) findViewById(R.id.button_signin);
        button_goRegister = (Button) findViewById(R.id.button_goRegister);

        button_signin.setOnClickListener(this);
        button_goRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.button_signin:
                startActivity(new Intent(this, MainActivity.class));
                break;

            case R.id.button_goRegister:
                startActivity(new Intent(this, Register.class));
                break;
        }
    }
}
