package com.example.aidavianisa.appberumon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends AppCompatActivity implements View.OnClickListener{

    Button button_signup, button_goLogin;
    EditText input_email, input_username, input_password, input_password2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        input_email = (EditText) findViewById(R.id.input_email);
        input_username = (EditText) findViewById(R.id.input_username);
        input_password = (EditText) findViewById(R.id.input_password);
        input_password2 = (EditText) findViewById(R.id.input_password2);
        button_signup = (Button) findViewById(R.id.button_signup);
        button_goLogin = (Button) findViewById(R.id.button_goLogin);

        button_signup.setOnClickListener(this);
        button_goLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.button_signup:
                startActivity(new Intent(this, Login.class));
                break;

            case R.id.button_goLogin:
                startActivity(new Intent(this, Login.class));
                break;
        }
    }
}
